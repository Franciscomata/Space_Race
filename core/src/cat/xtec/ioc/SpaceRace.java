package cat.xtec.ioc;

import com.badlogic.gdx.Game;

import cat.xtec.ioc.helpers.AssetManager;
import cat.xtec.ioc.screens.InicialScreen;

public class SpaceRace extends Game {

    @Override
    public void create() {

        AssetManager.load();
        setScreen(new InicialScreen(this));

    }
    @Override
    public void dispose() {
        super.dispose();
        AssetManager.dispose();
    }


}